import socket
import time
import requests
import requests_random_user_agent
from requests.exceptions import ConnectionError
from urllib.parse import urlparse
import tldextract
from datetime import datetime
import threading
from django.core.wsgi import get_wsgi_application
from sitemonitorconfig.models import *
from django.utils import timezone
from django.conf import settings
import django
from sitemonitor.settings import DATABASES, INSTALLED_APPS
import os
import sys



class SiteMonitor:

    __urls = []
    __processes = []
    __internet_connection = None
    __internet_connection_start_date = None
    __domains = []
    __status_page = None

    def __is_website_online(self, host):
        try:
            socket.gethostbyname(host)
        except socket.gaierror:
            return False
        else:
            return True

    def __get_site_status(self,url):
        try:
            result = requests.get(url)

            return result.status_code
        except:
            return False

    def __check_internet_connection(self):
        try:
            if self.__get_site_status('https://www.google.com/') != 200 \
                    or self.__get_site_status('https://www.yahoo.com/') != 200 \
                    or self.__get_site_status('https://www.microsoft.com/') != 200:
                return False
            return True
        except:
            return False

    def __run_processes(self):
        # Uruchamia procesy

        domains = Domain.objects.all()

        # sprawdzanie czy jakas domena nie doszla w miedzyczasie
        for domain in domains:
            if domain not in self.__domains:
                self.__domains.append(domain)

                process = threading.Thread(target=self.__process_management, args=(domain,))
                process.start()
                #self.__processes.append([url, process])

        # TODO::sprawdzanie czy jakas domena nie odeszla w miedzyczasie

    def __process_management(self, domain):

        #host = tldextract.extract(url)
        #host = host.domain + '.' + host.suffix

        while True:
            if self.__check_internet_connection():

                # zmiana flagi sprawdzajacej polaczenie z internetem
                if not self.__internet_connection:
                    # polaczenie internetowe zostalo przywrocone
                    self.__internet_connection = True
                    EventInternetConnection.objects.create(start_date=self.__internet_connection_start_date, end_date=timezone.now())
                    print('polaczenie internetowe przywrocone')
                    self.__internet_connection_start_date = None

                if self.__is_website_online(domain.get_host_name()):
                    # strona jest online

                    site_status = self.__get_site_status(domain.domain_name)

                    if site_status != 200:
                        # status strony inny niz 200
                        print('status strony != 200')
                        # TODO::inny status niz 200

                    elif not site_status:
                        # kiedy zwraca False - niespodziewany blad
                        print('ERROR')
                        # TODO::dodac do logera error z tego
                        # traktowac ten blad jak rozlaczenie z internetem, za chwile GLI sam do tego dojdzie
                    else:
                        # status 200 - prawidlowy
                        print('strona: ', domain.domain_name, ' OK!')

                else:
                    # strona nie jest online
                    print('strona nie jest online')

            else:
                # brak polaczenia z internetem
                self.__internet_connection = False
                self.__internet_connection_start_date = timezone.now()

                print('nie ma polaczenia z internetem')

            time.sleep(1)

    def __init__(self):

        self.__urls.append('https://neonail.pl/')
        self.__urls.append('https://klub.neonail.pl/')
        self.__urls.append('http://www.black-lashes.de')

        while True:
            self.__run_processes()

            # print('uruchomione procesy: ',self.__processes.__len__())
            # print('internet status: ', self.__internet_connection)

        # print('stop')


'''
        for url in self.__urls:
            # sprawdzanie czy process juz istnieje

            process_exist = False

            # sprawdzanie czy nie doszedl jakis nowy proces do uruchomienia (adres url)
            for process in self.__processes:
                if process[0] == url:
                    #process istnieje
                    process_exist = True

            # jezeli jest jakis nie uruchomiony proces - uruchamiamy go
            if not process_exist:
                # process nie istnieje
                process = threading.Thread(target=self.__process_management, args=(url,))
                process.start()
                self.__processes.append([url, process])

        # print('procesy uruchomione')


    def __process_management(self, url):

        host = tldextract.extract(url)
        host = host.domain + '.' + host.suffix

        while True:
            if self.__check_internet_connection():

                # zmiana flagi sprawdzajacej polaczenie z internetem
                if not self.__internet_connection:
                    # polaczenie internetowe zostalo przywrocone
                    self.__internet_connection = True
                    print('polaczenie internetowe przywrocone')
                    # TODO::dodac zmiane w DB

                if self.__is_website_online(host):
                    # strona jest online

                    site_status = self.__get_site_status(url)

                    if site_status != 200:
                        # status strony inny niz 200
                        print('status strony != 200')
                        # TODO::inny status niz 200

                    elif not site_status:
                        # kiedy zwraca False - niespodziewany blad
                        print('ERROR')
                        # TODO::dodac do logera error z tego
                        # traktowac ten blad jak rozlaczenie z internetem, za chwile GLI sam do tego dojdzie
                    else:
                        # status 200 - prawidlowy
                        print('strona: ',url,' OK!')

                else:
                    # strona nie jest online
                    print('strona nie jest online')

            else:
                # brak polaczenia z internetem
                self.__internet_connection = False
                print('nie ma polaczenia z internetem')

            time.sleep(1)
'''


settings.configure(DATABASES=DATABASES, INSTALLED_APPS=INSTALLED_APPS)
django.setup()
os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'sitemonitor.settings')


SiteMonitor()
