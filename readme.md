# Site Monitor

A simple website monitoring program - written in Python + Django (in progress).

The final version has the creation of reports access to websites and send a summary to the e-mail address.

## Main files
* `sitemonitorconfig/commangs/startmonitor.py` - main monitor script (multithreaded)
* `sitemonitorconfig/models.py` - project models
* `_temp` - temp files

## ToDo List:
* Logger system error
* Making reports
* Sending reports by e-mail in time cycles
* Tests
* Clean up  and create the final version (:

## Admin Panel
* `python3 manage.py createsuperuser` - add new Super User
* `sudo python3 manage.py startmonitor` - monitor start

## Docker

* `docker-compose up` - docker start
* `docker-composer build` - docker build