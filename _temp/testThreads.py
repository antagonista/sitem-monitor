import threading

def thread_test(n):
    i = 0
    while True:
        i += 1
        print(n,i)

if __name__ == "__main__":
    t1 = threading.Thread(target=thread_test, args=(1,))
    t2 = threading.Thread(target=thread_test, args=(2,))

    t1.start()
    t2.start()

