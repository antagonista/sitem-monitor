import SiteMonit
import requests
from datetime import datetime
import threading

class SiteMonitorAgent:

    __urls = []
    __internet_connection = None
    __internet_connection_time = None
    __processes = []

    def __get_site_status(self, url):
        result = requests.get(url)

        return result.status_code

    def __check_internet_connection(self):
        if self.__get_site_status('https://www.google.com/') != 200 \
                or self.__get_site_status('https://www.yahoo.com/') != 200 \
                or self.__get_site_status('https://www.microsoft.com/') != 200:
            return False
        return True

    def __start(self):
        while True:
            if (self.__check_internet_connection()):
                self.__internet_connection = True

                # TODO::sprawdzanie sledzenia zmian w urlach (jak zostanie dodany nowy w trakcie wykonywania programu)

                # tworzenie nowego procesu
                print('tworzenie procesu')
                print(self.__urls[0])
                process = SiteMonit.SiteMonitor(self.__urls[0])
                process = threading.Thread(target=process)
                process.start()
                print('process start')
                self.__processes.append(process)

                # obsluga istniejacych procesow

            else:
                # brak polaczenia z internetem
                self.__internet_connection = False
                # TODO::dodanie czasu zerwania polaczenia do bazy danych
                self.__internet_connection_time = datetime.now()

                while True:
                    if (self.__check_internet_connection()):
                        ## TODO::dodanie czasu powrocenia polaczenia do bazy danych
                        self.__internet_connection_time = None
                        break

    def __init__(self):
        self.__urls.append('https://neonail.pl/')
        self.__urls.append('https://klub.neonail.pl/')
        self.__urls.append('http://www.black-lashes.de')

        self.__start()

SiteMonitorAgent()