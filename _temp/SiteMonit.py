import socket
import time
import requests
import requests_random_user_agent
from requests.exceptions import ConnectionError
from urllib.parse import urlparse
import tldextract
from datetime import datetime


class SiteMonitor:

    __url = None
    __host = None
    __online = None
    __online_time = None
    __status = None
    __status_time = None
    __connection_error = None

    def __init__(self, url):
        self.__url = url
        host = tldextract.extract(self.__url)
        self.__host = host.domain + '.' + host.suffix

        self.__is_website_online()
        self.__get_site_status()

        #self.start()

    def __is_website_online(self):
        try:
            socket.gethostbyname(self.__host)
        except socket.gaierror:
            return False
        else:
            return True

    def __get_site_status(self):
        result = requests.get(self.__url)

        return result.status_code

    # GETTERS
    def get_status(self):
        return self.__status

    def get_status_time(self):
        return self.__status_time

    def get_online(self):
        return self.__online

    def get_online_time(self):
        return self.__online_time

    def get_connection_error(self):
        return self.__connection_error

    def get_url(self):
        return self.__url

    # Main function
    def start(self):
        while True:
            try:
                while True:

                    if (self.__is_website_online()):
                        # strona jest online - sprawdzanie statusu odpowiedzi http
                        self.__online = True
                        status = self.__get_site_status()
                        if (status != 200):
                            # inny status niz 200
                            self.__status = status
                            self.__status_time = datetime.now()

                            while True:
                                if (self.__get_site_status() == 200):
                                    self.__status_time = None
                                    break
                        else:
                            # poprawny status
                            self.__status = 200
                    else:
                        # stroa nie jest online

                        self.__online = False
                        self.__online_time = datetime.now()
                        while True:
                            if (self.__is_website_online()):
                                self.__online_time = None
                                # strona znow online
                                break

                    time.sleep(1)
                    self.__connection_error = False

            except ConnectionError:
                self.__connection_error = True