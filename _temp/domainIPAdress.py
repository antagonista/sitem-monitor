import requests
import requests_random_user_agent

# Script to get ip address server by domain

def check_peer(resp):
    orig_resp = resp.raw._original_response
    if hasattr(orig_resp, 'peer'):
        return getattr(orig_resp, 'peer')


domains = []

# domain list
domains.append('google.com')

count = 1

for domain in domains:
    domain = 'http://' + domain
    try:
        result = requests.get(domain, stream=True)
        print(count, ';', domain, ';', result.raw._original_response.fp.raw._sock.getpeername()[0])
    except:
        print(count, ';', domain, ';', 'ERROR')

    count += 1