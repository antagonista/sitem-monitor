from django.contrib import admin
from .models import Domain, EmailAddress

# Register your models here.

admin.site.register(Domain)
admin.site.register(EmailAddress)
