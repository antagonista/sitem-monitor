from django.core.management.base import BaseCommand, CommandError
import socket
import time
import requests
import requests_random_user_agent
from requests.exceptions import ConnectionError
from urllib.parse import urlparse
import tldextract
from datetime import datetime
import threading
from sitemonitorconfig.models import *
from django.utils import timezone


class SiteMonitor:

    __urls = []
    __processes = []
    __domains = []
    __internet_connection = 'start'
    __internet_connection_start_date = 'null'
    __status_page = None

    def __is_website_online(self, host):
        try:
            socket.gethostbyname(host)
        except socket.gaierror:
            return False
        else:
            return True

    def __get_site_status(self,url):
        try:
            result = requests.get(url)

            return result.status_code
        except:
            return False

    def __check_internet_connection(self):
        try:
            if self.__get_site_status('https://www.google.com/') != 200 \
                    or self.__get_site_status('https://www.yahoo.com/') != 200 \
                    or self.__get_site_status('https://www.microsoft.com/') != 200:
                return False
            return True
        except:
            return False

    def __run_processes(self):
        # Uruchamia procesy

        domains = Domain.objects.all()

        # sprawdzanie czy jakas domena nie doszla w miedzyczasie
        for domain in domains:
            if domain not in self.__domains:
                self.__domains.append(domain)

                process = threading.Thread(target=self.__process_management, args=(domain,))
                process.start()
                self.__processes.append([domain, process])

        # TODO::sprawdzanie czy jakas domena nie odeszla w miedzyczasie
        #print('domen: ', len(domains), 'procesow: ', len(self.__processes))

    def __checking_internet_connection(self):

        if self.__check_internet_connection():
            # jest polaczenie internetowe

            if self.__internet_connection == 'start':
                # uruchomienie demona - ustawienie flagi
                self.__internet_connection = True

            if self.__internet_connection == False:
                # polaczenie internetowe powrocilo
                EventInternetConnection.objects.create(start_date=self.__internet_connection_start_date,
                                                       end_date=timezone.now())
                print('polaczenie internetowe przywrocone')
                self.__internet_connection_start_date = 'null'
                self.__internet_connection = True

            return True
        else:
            # nie ma polaczenia internetowego
            self.__internet_connection = False
            if self.__internet_connection_start_date is 'null':
                self.__internet_connection_start_date = timezone.now()

            print('nie ma polaczenia z internetem')

            return False

    def __checking_page_status(self,domain):
        site_status = self.__get_site_status(domain.domain_name)

        if site_status != 200:
            # status strony inny niz 200
            print('status strony != 200')
            # TODO::inny status niz 200
            return False

        elif not site_status:
            # kiedy zwraca False - niespodziewany blad
            print('ERROR')
            # TODO::dodac do logera error z tego
            # traktowac ten blad jak rozlaczenie z internetem, za chwile GLI sam do tego dojdzie
            return False
        else:
            # status 200 - prawidlowy
            return True

    def __process_management(self, domain):

        start_data_website_online_event = None
        start_data_status_change_event = None
        status_change_value = None
        process_status = True

        while True:

            # TODO:: sprawdzanie czy proces powinien istniec (po domenach)

            if self.__checking_internet_connection():
                # jest polaczenie internetowe

                if self.__is_website_online(domain.get_host_name()):
                    # strona internetowa jest online

                    if start_data_website_online_event is not None:
                        # strona znow stala sie online
                        EventDomain(domain=domain,
                                    type_event=1,
                                    start_date=start_data_website_online_event,
                                    end_date=timezone.now())
                        start_data_website_online_event = None
                        print('strona znow online')

                    # PAGE STATUS
                    site_status = self.__get_site_status(domain.domain_name)

                    if site_status is not 200:
                        # status strony inny niz 200
                        print('status strony != 200')

                        if status_change_value is not None and status_change_value is not site_status:
                            # z nieprawidlowego na inny nieprawidlowy
                            EventDomain(domain=domain,
                                        type_event=2,
                                        start_date=start_data_status_change_event,
                                        end_date=timezone.now(),
                                        description_status=status_change_value)
                            start_data_status_change_event = timezone.now()
                            status_change_value = site_status

                        elif status_change_value is None and status_change_value is None:
                            # zmiana z prawidlowego na nieprawidlowy status
                            status_change_value = site_status
                            start_data_status_change_event = timezone.now

                    elif not site_status:
                        # kiedy zwraca False - niespodziewany blad
                        print('ERROR')
                        # TODO::dodac do logera error z tego
                        # traktowac ten blad jak rozlaczenie z internetem, za chwile GLI sam do tego dojdzie
                    else:
                        # status 200 - prawidlowy
                        #print('strona: ', domain.domain_name, ' OK!')

                        if start_data_status_change_event is not None and start_data_status_change_event is not None:
                            # nastapila zmiana statusu z nieprawidlowego na prawidlowy
                            EventDomain(domain=domain,
                                        type_event=2,
                                        start_date=start_data_status_change_event,
                                        end_date=timezone.now(),
                                        description_status=status_change_value)
                            start_data_status_change_event = None
                            status_change_value = None

                    # END PAGE STATUS
                else:
                    # strona internetowa jest offline
                    print('strona nie jest online: ', domain.domain_name)
                    if start_data_website_online_event is None:
                        start_data_website_online_event = timezone.now()

            time.sleep(1)

            '''
            if self.__check_internet_connection():

                # zmiana flagi sprawdzajacej polaczenie z internetem
                if not self.__internet_connection:

                    if self.__internet_connection is None:
                        # ustawienie flagi po starcie programu
                        self.__internet_connection = True

                    if not self.__internet_connection:
                        # polaczenie internetowe zostalo przywrocone
                        self.__internet_connection = True
                        EventInternetConnection.objects.create(start_date=self.__internet_connection_start_date,
                                                               end_date=timezone.now())
                        print('polaczenie internetowe przywrocone')
                        self.__internet_connection_start_date = None

                if self.__is_website_online(domain.get_host_name()):
                    # strona jest online

                    site_status = self.__get_site_status(domain.domain_name)

                    if site_status != 200:
                        # status strony inny niz 200
                        print('status strony != 200')
                        # TODO::inny status niz 200

                    elif not site_status:
                        # kiedy zwraca False - niespodziewany blad
                        print('ERROR')
                        # TODO::dodac do logera error z tego
                        # traktowac ten blad jak rozlaczenie z internetem, za chwile GLI sam do tego dojdzie
                    else:
                        # status 200 - prawidlowy
                        print('strona: ', domain.domain_name, ' OK!')

                else:
                    # strona nie jest online
                    print('strona nie jest online')

            else:
                # brak polaczenia z internetem
                self.__internet_connection = False
                if self.__internet_connection_start_date is None:
                    self.__internet_connection_start_date = timezone.now()

                print('nie ma polaczenia z internetem')

            time.sleep(1)
            '''

    def __init__(self):

        while True:
            self.__run_processes()

            # print('uruchomione procesy: ',self.__processes.__len__())
            # print('internet status: ', self.__internet_connection)

        # print('stop')


'''
        for url in self.__urls:
            # sprawdzanie czy process juz istnieje

            process_exist = False

            # sprawdzanie czy nie doszedl jakis nowy proces do uruchomienia (adres url)
            for process in self.__processes:
                if process[0] == url:
                    #process istnieje
                    process_exist = True

            # jezeli jest jakis nie uruchomiony proces - uruchamiamy go
            if not process_exist:
                # process nie istnieje
                process = threading.Thread(target=self.__process_management, args=(url,))
                process.start()
                self.__processes.append([url, process])

        # print('procesy uruchomione')


    def __process_management(self, url):

        host = tldextract.extract(url)
        host = host.domain + '.' + host.suffix

        while True:
            if self.__check_internet_connection():

                # zmiana flagi sprawdzajacej polaczenie z internetem
                if not self.__internet_connection:
                    # polaczenie internetowe zostalo przywrocone
                    self.__internet_connection = True
                    print('polaczenie internetowe przywrocone')
                    # TODO::dodac zmiane w DB

                if self.__is_website_online(host):
                    # strona jest online

                    site_status = self.__get_site_status(url)

                    if site_status != 200:
                        # status strony inny niz 200
                        print('status strony != 200')
                        # TODO::inny status niz 200

                    elif not site_status:
                        # kiedy zwraca False - niespodziewany blad
                        print('ERROR')
                        # TODO::dodac do logera error z tego
                        # traktowac ten blad jak rozlaczenie z internetem, za chwile GLI sam do tego dojdzie
                    else:
                        # status 200 - prawidlowy
                        print('strona: ',url,' OK!')

                else:
                    # strona nie jest online
                    print('strona nie jest online')

            else:
                # brak polaczenia z internetem
                self.__internet_connection = False
                print('nie ma polaczenia z internetem')

            time.sleep(1)
'''


class Command(BaseCommand):
    help = 'Run Site Monitor Deamon'

    def handle(self, *args, **options):
        SiteMonitor()

