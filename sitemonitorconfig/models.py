from django.db import models
from tldextract import tldextract


class Domain(models.Model):
    domain_name = models.CharField(max_length=80)

    def get_host_name(self):
        host = tldextract.extract(self.domain_name)
        host = host.domain + '.' + host.suffix
        return host

    def __str__(self):
        return self.domain_name


class EmailAddress(models.Model):
    e_mail = models.CharField(max_length=255)


class EmailAddressToDomain(models.Model):
    domain = models.ForeignKey('Domain', on_delete=models.CASCADE)
    e_mail = models.ForeignKey('EmailAddress', on_delete=models.CASCADE)


class EventDomain(models.Model):
    domain = models.ForeignKey('Domain', on_delete=models.CASCADE)
    type_event = models.IntegerField() #1 - online website event; 2 - status page event
    start_date = models.DateTimeField(default=None, blank=True, null=True)
    end_date = models.DateTimeField(default=None, blank=True, null=True)
    description_status = models.IntegerField(default=None, blank=True, null=True)


class EventInternetConnection(models.Model):
    start_date = models.DateTimeField(default=None, blank=True, null=True)
    end_date = models.DateTimeField(default=None, blank=True, null=True)
